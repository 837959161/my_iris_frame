package main


import (
	stdContext "context"
	//"unicode"
	"os"
	"os/signal"
	"syscall"
	"time"
    "fmt"
    "github.com/kataras/iris"
    "github.com/kataras/iris/sessions"
    "github.com/kataras/iris/mvc"
)

func main() {
    app := iris.New()
    app.Logger().SetLevel("debug")

    mvc.Configure(app.Party("/basic"), basicMVC)
	// app.Get("/", func(ctx iris.Context) {
	// 	str := "A2BC6DF@"
	// 	newStr := ""
	// 	num := 3

	// 	for i := 0; i < len(str); i++ {
	// 		ch := str[i]
	// 		if(ch != '@'){
			
	// 			if(unicode.IsDigit(ch)){
	// 				for j :=0 ; j<num;j++ {
	// 					newStr += str[i+1]
	// 					if j == 1{
	// 						newStr += '_'
	// 					}
					

	// 				}

	//         	}else{
	//         		newStr += ch
	//         	}
	// 		}   
 //    }

 //        ctx.Writef("Hello from the server"+newStr)
 //    })


    go func() {
        ch := make(chan os.Signal, 1)
        signal.Notify(ch,
            // kill -SIGINT XXXX 或 Ctrl+c
            os.Interrupt,
            syscall.SIGINT, // register that too, it should be ok
            // os.Kill等同于syscall.Kill
            os.Kill,
            syscall.SIGKILL, // register that too, it should be ok
            // kill -SIGTERM XXXX
            syscall.SIGTERM,
        )
        select {
        case <-ch:
            println("shutdown...")
            timeout := 5 * time.Second
            ctx, cancel := stdContext.WithTimeout(stdContext.Background(), timeout)
            defer cancel()
            app.Shutdown(ctx)
        }
    }()
    app.Run(iris.Addr(":9000"),iris.WithoutInterruptHandler)
}

func basicMVC(app *mvc.Application) {
	app.Router.Use(func(ctx iris.Context) {
        ctx.Application().Logger().Infof("Path: %s", ctx.Path())
        ctx.Next()
    })

    // app.Register(
    //     sessions.New(sessions.Config{}).Start,
    //     &prefixedLogger{prefix: "DEV"},
    // )

     app.Handle(new(basicController))
     app.Party("/sub").Handle(new(basicSubController))

}


type LoggerService interface {
    Log(string)
}
type prefixedLogger struct {
    prefix string
}

func (s *prefixedLogger) Log(msg string) {
    fmt.Printf("%s: %s\n", s.prefix, msg)
}

type basicController struct {
    Logger LoggerService
    Session *sessions.Session
}
func (c *basicController) BeforeActivation(b mvc.BeforeActivation) {
    b.Handle("GET", "/custom", "Custom")
}

func (c *basicController) AfterActivation(a mvc.AfterActivation) {
    if a.Singleton() {
        panic("basicController should be stateless,a request-scoped,we have a 'Session' which depends on the context.")
    }
}

func (c *basicController) Get() string {
    count := c.Session.Increment("count", 1)
    body := fmt.Sprintf("Hello from basicController\nTotal visits from you: %d", count)
    c.Logger.Log(body)
    return body
}

func (c *basicController) Custom() string {
    return "custom"
}

type basicSubController struct {
    Session *sessions.Session
}

func (c *basicSubController) Get() string {
    count := c.Session.GetIntDefault("count", 1)
    return fmt.Sprintf("Hello from basicSubController.\nRead-only visits count: %d", count)
}