package controllers

import (
	"../../model"
    "../../service"
    "github.com/kataras/iris"
    "github.com/kataras/iris/mvc"
    "github.com/kataras/iris/sessions"
)


type UserController struct {
	Ctx iris.Context
	Service service.UserService
	Session *session.Session
}

const userIdKey = "UserId"

func (c *UserController) getUserId() int64 {
	userId := c.Session.GetInt64Default(userIdKey,0)
	return userID
}

func (c *UserController) Get() (results []model.User) {
	return c.Service.GetAll()
}

func (c *UserController) logout() {
	c.Session.Destroy()
}


//用户注册
func (c *UserController) GetRegister() mvc.Result {
	if c.getUserId() {
		c.logout()
	}
	return mvc.View{
	    Name: "user/register.html",
	    Data: iris.Map{"Title": "用户注册"},
	}
}

//用户注册处理
func (c *UserController) PostRegister() mvc.Result {
	var (
        nickname = c.Ctx.FormValue("nickname")
        username  = c.Ctx.FormValue("username")
        password  = c.Ctx.FormValue("password")
    )

    //创建新用户，密码将由服务进行哈希处理
    u, err := c.Service.Create(password, model.User{
        Username:  username,
        Nickname: nickname,
    })

    c.Session.Set(userIDKey, u.ID)
    return mvc.Response{
        //如果不是nil，则会显示此错误
        Err: err,
        //从定向 /user/me.
        Path: "/user/me",
      
    }
}

