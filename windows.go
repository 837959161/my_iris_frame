package main
 
import (
    "github.com/sciter-sdk/go-sciter"
    "github.com/sciter-sdk/go-sciter/window"
    "log"
    "fmt"
    "encoding/json"
)




type User struct {
	Name string `json:"name"`;
	Pwd string `json:"pwd"`;
}

func defFunc(w *window.Window) {

	//注册dump函数方便在tis脚本中打印数据
	w.DefineFunction("dump", func(args ...*sciter.Value) *sciter.Value {
		for _, v := range args {
			fmt.Print(v.String() + " ");
		}
		fmt.Println("sd");
		return sciter.NullValue();
	});

	//定义函数，用于tis脚本中加载用户名和密码
	w.DefineFunction("LoadNameAndPwd", func(args ...*sciter.Value) *sciter.Value {

		//查询一行数据
		name := "小李";
		pwd := "1212";

		//构建一个json
		user := User{Name: name, Pwd: pwd};
		data, _ := json.Marshal(user);

		//将json返回，tis脚本中接收该值
		return sciter.NewValue(string(data));
	});
}


 
func main() {
    //创建新窗口
	//并设置窗口大小
	w, err := window.New(sciter.DefaultWindowCreateFlag, &sciter.Rect{200, 200, 800, 800});
	if err != nil {
		log.Fatal(err);
	}
	//加载文件
	w.LoadFile("E:/go/src/my_iris_frame/demo1.html");
	//设置标题
	w.SetTitle("固定大小窗口");
	//定义函数
	defFunc(w);
	//显示窗口
	w.Show();
	//运行窗口，进入消息循环
	w.Run();
}