
  package main

import (
	"bufio"
	"encoding/json"
	////"errors"
	//"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"regexp"
	"strconv"
	"strings"
	"syscall"
	"time"

	//"github.com/influxdata/influxdb/client/v2"
)

type Reader interface {
	Read(rc chan []byte)
}


type LogProcess struct {
	rc     chan []byte
	reader Reader
}

type ReadFromTail struct {
	inode uint64
	fd    *os.File
	path  string
}


type Message struct {
	TimeLocal                    time.Time
	BytesSent                    int
	Path, Method, Scheme, Status string
	UpstreamTime, RequestTime    float64
}

type Monitor struct {
	listenPort string
	startTime  time.Time
	tpsSli     []int
	systemInfo SystemInfo
}

type SystemInfo struct {
	HandleLine   int       `json:"handleLine"`   // 已经处理的日志行数
	Tps          float64   `json:"tps"`          // 系统吞出量
	ReadChanLen  int       `json:"readChanLen"`  // input channel 长度
	RunTime      string    `json:"runTime"`      // 运行总时间
	ErrInfo      ErrorInfo `json:"errInfo"`      // 错误信息
}

type ErrorInfo struct {
	ReadErr    int `json:"readErr"`
	ProcessErr int `json:"processErr"`
}

type TypeMonitor int

const (
	TypeHandleLine TypeMonitor = iota
	TypeReadErr
	TypeProcessErr
)

var (
	path, influxDsn, listenPort string
	processNum, writeNum        int
	TypeMonitorChan             = make(chan TypeMonitor, 200)
)



func NewLogProcess(reader Reader) *LogProcess {
	return &LogProcess{
		rc:     make(chan []byte, 200),
		reader: reader,
	}
}

func (l *LogProcess) Process() {
	/**
	'$remote_addr\t$http_x_forwarded_for\t$remote_user\t[$time_local]\t$scheme\t"$request"\t$status\t$body_bytes_sent\t"$http_referer"\t"$http_user_agent"\t"$gzip_ratio"\t$upstream_response_time\t$request_time'
	*/

	rep := regexp.MustCompile(`([\d\.]+)\s+([^ \[]+)\s+([^ \[]+)\s+\[([^\]]+)\]\s+([a-z]+)\s+\"([^"]+)\"\s+(\d{3})\s+(\d+)\s+\"([^"]+)\"\s+\"(.*?)\"\s+\"([\d\.-]+)\"\s+([\d\.-]+)\s+([\d\.-]+)`)

	loc, _ := time.LoadLocation("Asia/Shanghai")
	for v := range l.rc {
		TypeMonitorChan <- TypeHandleLine
		ret := rep.FindStringSubmatch(string(v))
		if len(ret) < 13 {
			log.Println("wrong input data:", v)
			TypeMonitorChan <- TypeProcessErr
			continue
		}

		timeLocal, err := time.ParseInLocation("02/Jan/2006:15:04:05 +0000", ret[4], loc)
		if err != nil {
			TypeMonitorChan <- TypeProcessErr
			log.Println("time parse error:", err)
			continue
		}

		request := ret[6]
		requestSli := strings.Split(request, " ")
		if len(requestSli) < 3 {
			TypeMonitorChan <- TypeProcessErr
			log.Println("input request wrong:", request)
			continue
		}
		method := strings.TrimLeft(requestSli[0], "\"")
		u, err := url.Parse(requestSli[1])
		if err != nil {
			TypeMonitorChan <- TypeProcessErr
			log.Println("input url parse error:", err)
			continue
		}
		path := u.Path
		scheme := ret[5]
		status := ret[7]
		bytesSent, _ := strconv.Atoi(ret[8])
		upstreamTime, _ := strconv.ParseFloat(ret[12], 64)
		requestTime, _ := strconv.ParseFloat(ret[13], 64)

	
	}
}

func (m *Monitor) start(lp *LogProcess) {
	// 消费监控数据
	go func() {
		for n := range TypeMonitorChan {
			switch n {
			case TypeHandleLine:
				m.systemInfo.HandleLine += 1
			case TypeReadErr:
				m.systemInfo.ErrInfo.ReadErr += 1
			case TypeProcessErr:
				m.systemInfo.ErrInfo.ProcessErr += 1
			}
		}
	}()

	ticker := time.NewTicker(time.Second * 5)
	// 计算TPS
	go func() {
		for {
			<-ticker.C
			m.tpsSli = append(m.tpsSli, m.systemInfo.HandleLine)
			if len(m.tpsSli) > 2 {
				m.tpsSli = m.tpsSli[1:]
			}
		}
	}()

	http.HandleFunc("/monitor", func(writer http.ResponseWriter, request *http.Request) {
		io.WriteString(writer, m.systemStatus(lp))
	})
	log.Fatal(http.ListenAndServe(":"+m.listenPort, nil))
}

func (m *Monitor) systemStatus(lp *LogProcess) string {
	d := time.Now().Sub(m.startTime)
	m.systemInfo.RunTime = d.String()
	m.systemInfo.ReadChanLen = len(lp.rc)
	if len(m.tpsSli) >= 2 {
		// return math.Trunc(float64(m.tpsSli[1]-m.tpsSli[0])/5*1e3+0.5) * 1e-3
		m.systemInfo.Tps = float64(m.tpsSli[1]-m.tpsSli[0]) / 5
	}
	res, _ := json.MarshalIndent(m.systemInfo, "", "\t")
	return string(res)
}

func (r *ReadFromTail) Read(rc chan []byte) {
	defer close(rc)
	var stat syscall.Stat_t

	r.fd.Seek(0, 2) // seek 到末尾
	bf := bufio.NewReader(r.fd)

	for {
		line, err := bf.ReadBytes('\n')
		if err == io.EOF {
			if err := syscall.Stat(r.path, &stat); err != nil {
				// 文件切割，但新文件还没有生成
				time.Sleep(1 * time.Second)
			} else {
				nowInode := stat.Ino
				if nowInode == r.inode {
					// 无新的数据产生
					time.Sleep(1 * time.Second)
				} else {
					// 文件切割，重新打开文件
					r.fd.Close()
					fd, err := os.Open(r.path)
					if err != nil {
						panic(fmt.Sprintf("Open file err: %s", err.Error()))
					}
					r.fd = fd
					bf = bufio.NewReader(fd)
					r.inode = nowInode
				}
			}
			continue
		} else if err != nil {
			log.Printf("readFromTail ReadBytes err: %s", err.Error())
			TypeMonitorChan <- TypeReadErr
			continue
		}

		rc <- line[:len(line)-1]
	}
}



func main() {

	lp := NewLogProcess(reader)

	go lp.reader.Read(lp.rc)

	for i := 1; i <= processNum; i++ {
		go lp.Process()
	}

	m := &Monitor{
		listenPort: listenPort,
		startTime:  time.Now(),
	}
	go m.start(lp)

	c := make(chan os.Signal)
	signal.Notify(c, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT, syscall.SIGUSR1)
	for s := range c {
		switch s {
		case syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT:
			log.Println("capture exit signal:", s)
			os.Exit(1)
		case syscall.SIGUSR1: // 用户自定义信号
			log.Println(m.systemStatus(lp))
		default:
			log.Println("capture other signal:", s)
		}
	}
}
